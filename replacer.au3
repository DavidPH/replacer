#include <HotString.au3>
#include <File.au3>

$file = "replacer.txt"
Global $numL = _FileCountLines($file)
If $numL <> 0 Then
   Global $arr[$numL/2]
   Global $replies[$numL/2]
   FileOpen($file, 0)
   $count = 0
   For $i = 1 to $numL Step 2
	  $arr[$count] = FileReadLine($file, $i)
	  $replies[$count] = FileReadLine($file, $i + 1)
	  $count = $count + 1
   Next
   for $i = 0 to UBound($arr) -1
	  HotStringSet($arr[$i], replace)
   Next
   FileClose($file)
EndIf

Func replace($caller)
   $i = _ArraySearch($arr, $caller)
   send('{BS ' & StringLen($caller) & '}' & $replies[$i])
EndFunc

While 1
Sleep(10)
WEnd